CC=gcc
CFLAGS=-std=gnu99 -Wall -fgnu-tm -lrt -g
LDFLAGS=-fgnu-tm -lcrypto
LDFLAGS2=-lrt
SOURCES=$(wildcard *.c)
OBJECTS=$(SOURCES:.c=.o)

TARGETS=test-reset run-transactions cancel-commit
COMMON_DEP=reset-safe.h reset-safe.o 

all: $(OBJECTS) $(TARGETS) 

test-reset: test-reset.o $(COMMON_DEP)
	$(CC) $(LDFLAGS) $^ $(LDFLAGS2) -o $@ 

cancel-commit: cancel-commit.o encryptSign.o $(COMMON_DEP)
	$(CC) $(LDFLAGS) $^ -o $@ $(LDFLAGS2) -lcrypto

run-transactions: run-transactions.o $(COMMON_DEP)
	$(CC) $(LDFLAGS) $^ -o $@ $(LDFLAGS2) 

.c.o:
	$(CC) -c $(CFLAGS)  $< 

.PHONY: all clean
.DEFAULT: all

clean:
	@-rm -f $(OBJECTS) *.o
	@-rm -f *~ \#*
	@-rm -f $(TARGETS)
