#include <stdio.h>
#include <stdbool.h>
#include "reset-safe.h"
#include "gettime.h"

#define DEBUG false
#define RUNS 20
#define FXN  encryptSign()

// Calculate the median value from an array of ulongs
unsigned long median(int n, unsigned long x[]) {
  unsigned long temp;
  // the following two loops sort the array x in ascending order
  for(int i=0; i<n-1; i++) {
    for(int j=i+1; j<n; j++) {
      if(x[j] < x[i]) {
	// swap elements
	temp = x[i];
	x[i] = x[j];
	x[j] = temp;
      }
    }
  }
 
  if(n%2==0) {
    // if there is an even number of elements, return mean of the two elements in the middle
    return((x[n/2] + x[n/2 - 1]) / 2.0);
  } else {
    // else return the element in the middle
    return x[n/2];
  }
}

// Run our benchmarks
int main()
{
  int i;
  unsigned long start,end,time;
  int data_size = RUNS*sizeof(unsigned long);

  unsigned long *data = malloc(data_size);
  if (data == NULL) 
    {
      printf("Unable to allocate memory!\n");
      exit(1);
    }
  
  //-----------------------------------------CANCEL-----------------------------------
  memset(data, 0, data_size);
  for(i=0;i<RUNS+1;i++)
    {
      start=get_time();
      __transaction_atomic 
	{
 	  FXN;
	  __transaction_cancel;
	}
      end=get_time();
      time=end-start;
      if (DEBUG) printf("Cancel Time is %lu \n",time);
      if(i>0)
	data[i-1]=time;
    }
  printf("median Cancel Time is %lu \n",median(RUNS,data));
  
  //--------------------------------------TM---------------------------------------
  memset(data, 0, data_size);
  for(i=0;i<RUNS+1;i++)
    {                                                                                              
      start=get_time();
      __transaction_atomic { FXN; }
      end=get_time();
      time=end-start;
      
      if (DEBUG) printf("TM Time is %lu \n",time);
      if(i>0)
        data[i-1]=time;
    }
  printf("median TM Time is %lu \n",median(RUNS,data));
  //--------------------------------------BARE---------------------------------------
  memset(data, 0, data_size);                                                             
  for(i=0;i<RUNS+1;i++)
    {
      start=get_time();
      FXN; 
      end=get_time();
      time=end-start;
      if (DEBUG) printf("BARE Time is %lu \n",time);
      if(i>0)
        data[i-1]=time;
    }
  printf("Median BARE Time is %lu \n",median(RUNS,data));  
  return(0);
}
