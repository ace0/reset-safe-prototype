#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <openssl/aes.h>
#include <openssl/dsa.h>
#include <openssl/md5.h>

// Turn on/off debugging output
#undef DEBUG_ENCRYPT    
//#define DEBUG_ENCRYPT 

#define RNG_FILE "/dev/urandom"
#define IV_SIZE        (128/8) // bytes
#define AES_KEY_SIZE   128     // bits
#define DSA_KEY_SIZE   1024    // bits
#define HASH_SIZE      16      // bytes
#define SIGNATURE_SIZE 48      // bytes

/* Read random bytes from the OS RNG */
__attribute__((transaction_pure))
void get_random_bytes(unsigned char* const data, const unsigned int bytes)
{
  // Opent the RNG and read the requested number of bytes
  FILE* RNG = fopen(RNG_FILE, "r");
  fread(data, 0, bytes, RNG);
  fclose(RNG);
}

/* Encrypt 1 block of data with AES-128 */
__attribute__((transaction_pure))
void encrypt(const unsigned char* const ckey, 
	     unsigned char* const iv, 
	     const unsigned char* const in, 
	     unsigned char* const out)
{
  AES_KEY key;
  int num = 0;
  AES_set_encrypt_key(ckey, AES_KEY_SIZE, &key);
  AES_cfb128_encrypt(in, out, AES_BLOCK_SIZE, &key, iv, &num, AES_ENCRYPT);
}

/* Report a critical error and exit the application */
__attribute__((transaction_pure))
void critical(const char* const message, ...)
{
  // Initialize the variable argument list.
  va_list args;
  va_start(args, message);

  // Print our error message.
  vfprintf(stderr, message, args);
  fprintf(stderr, "\n");
  
  // Cleanup and exit.
  va_end(args);
  exit(0);  
}

/* Generate DSA digital signature over an input */
__attribute__((transaction_pure))
void sign(const unsigned char* const data, 
	  const unsigned int size,
	  unsigned char* const seed,
	  unsigned char* const signature)
{
  unsigned char hash[HASH_SIZE];
  unsigned int signature_size;

  // Generate DSA parameters.
  DSA* dsa = DSA_generate_parameters(DSA_KEY_SIZE,seed,IV_SIZE,NULL,NULL,NULL,NULL); 
  if (dsa == NULL) critical("Failed to generate DSA parameters.");

  // Generate a DSA key.
  if (!DSA_generate_key(dsa)) critical("Failed to generate DSA key.");

  // Generate the digital signature.
  MD5(data, size, NULL);
  int status = 
    DSA_sign(0, hash, HASH_SIZE, signature, &signature_size, dsa);
  if (!status) critical("Failed to generate digitial signature.  Error code %d", status);
}

/* Reads random values from the RNG, encrypts, and digitally signs a single
   block (128-bits) of data */
//#ifndef DEBUG_ENCRYPT
__attribute__((transaction_safe))
//#endif
void encryptSign()
{
  unsigned char plaintext[AES_BLOCK_SIZE] = "Plaintext data";
  unsigned char ciphertext[AES_BLOCK_SIZE];
  unsigned char signature[SIGNATURE_SIZE];
  unsigned char ckey[] =  "This is an encryption key believe it or not.";
  
  // Start fresh!
  memset(ciphertext, 0, AES_BLOCK_SIZE);
  memset(signature, 0, SIGNATURE_SIZE);

  // Read an initialization vector from the Linux RNG for AES.
  unsigned char iv[IV_SIZE];
  get_random_bytes(iv, IV_SIZE);  

  // Get random DSA seed.
  unsigned char seed[IV_SIZE];
  get_random_bytes(seed, IV_SIZE);

  /* Encrypt then sign the input */
  encrypt(ckey, iv, plaintext, ciphertext);
  sign(ciphertext, AES_BLOCK_SIZE, seed, signature);

#ifdef DEBUG_ENCRYPT
  /* Print encrypted/signed data */
  printf("Ciphertext: ");
  for (int i=0; i<AES_BLOCK_SIZE; ++i)
    printf("%02X", ciphertext[i]);
  printf("\n");

  printf("Signature: ");
  for (int i=0; i<SIGNATURE_SIZE; ++i)
    printf("%02X", signature[i]);
  printf("\n");
#endif
}
