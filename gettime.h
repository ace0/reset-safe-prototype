#ifndef _MY_GETTIME_H
#define _MY_GETTIME_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

static unsigned long inline get_time()
{
  int r;
  struct timespec time;
  r = clock_gettime(CLOCK_REALTIME, &time);
  if (r < 0 )
    {
      printf("clock_gettimg failed!\n");
      exit(1);
    }
  return (time.tv_sec * 1000*1000*1000 + time.tv_nsec);
}

#endif
