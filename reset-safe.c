#define _GNU_SOURCE
#include <stdio.h>
#include <stdbool.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <sys/syscall.h>
#include "reset-safe.h"

/**
 * Get the reset counter using the specified mode (see reset-safe.h for modes).
 */
__attribute__((transaction_pure))
long get_reset_count(int mode)
{
  // Normal functionality
  if (mode == SYSCALL_HYPERCALL)
    { 
      // Get the reset counter from the OS/Hypervisor.
      return syscall(__NR_get_vm_rcnt);
    }

  // Simulate the reset count using our built-in profile.
  if (mode == SIMULATE_RC)
    {
      static int i = 0;
      // Simulate a reset.
      switch (++i)
	{
	case 1: return 1;
	case 2: return 1;  // No Reset
	case 3: return 1;
	case 4: return 1;  // No reset
	default: return 1; // No reset
	}
    }
     
  // Otherwise, use a dummy system call and return a constant value
  else
    { 
      syscall(__NR_nop);
      return 1;
    }
}

/* Use printf from within an atomic transaction */
__attribute__((transaction_pure))
void safe_print(const char* format, ...)
{
  // Initialize variable arguments list.
  va_list args;
  va_start(args, format);

  // Print the message following by a newline.
  vprintf(format, args);
  printf("\n");
  fflush(stdout);
  
  va_end(args); // Cleanup our variable argument list.
}

/* Prints a message if the debug output is turned on */
__attribute__((transaction_pure))
void dprint(const char* format, ...)
{
  // Quit if debug output is disabled.
  if (!DEBUG) return;

  // Initialize variable arguments list.
  va_list args;
  va_start(args, format);
  safe_print(format, args);
  va_end(args);
}

/* Allow sleeping inside a transaction */
__attribute__((transaction_pure))
void safe_sleep(unsigned int seconds)
{
  sleep(seconds);  
}

/**
 * Runs any function inside of a GCC atomic transaction- 
 * and guarantess that the transaction will be aborted (and retried)
 * if a VM reset-from-snapshot occurs before the transaction can be 
 * committed.
 */
void run_reset_atomic(__attribute__((transaction_safe)) void(*fn_ptr)(), int mode)
{
  bool commit = false;
  while (!commit)
    {
      // Get the current vm reset counter.
      int rc = get_reset_count(mode);

      // Start an atomic transaction
      __transaction_atomic
	{
	  // Run the desired code.
	  fn_ptr();
  
	  // Check to see if the vm reset counter has changed.
	  if (rc != get_reset_count(mode))
	    {
	      // Cancel this transaction.
	      dprint("Canceling this transaction");
	      __transaction_cancel;
	    }
	  
	  else
	    {
	      // Commit this transaction.
	      dprint("Commiting this transaction");
	      commit = 1;
	    }
	}
    }
}

