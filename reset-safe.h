#ifndef _RESET_SAFE_H
#define _RESET_SAFE_H

#define __NR_get_vm_rcnt 313   // 64-bit syscall number
#define __NR_nop         314   // 64-bit syscall number

#define DEBUG false            // Show debugging output

// Modes for run reset atomic
#define SYSCALL_HYPERCALL 0  // Use syscall-hypercall
#define SIMULATE_RC       1  // Use built-in simulated reset counter
#define DUMMY_SC          2  // Use a dummy system call and return a const value
#define BASE              3  // Don't use reset-atomic at all.

__attribute__((transaction_pure))
long get_reset_count();

__attribute__((transaction_pure))
void safe_print(const char* format, ...);

__attribute__((transaction_pure))
void dprint(const char* format, ...);

__attribute__((transaction_pure))
void safe_sleep(unsigned int seconds);

__attribute__((transaction_safe))
void nqueens();

__attribute__((transaction_safe))
void encryptSign();

void run_reset_atomic(void (*fn_ptr)(void), int mode);

#endif
