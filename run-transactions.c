#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include "reset-safe.h"

#define SLEEP 8

// Run a single transaction and sleep in the middle.
__attribute__((transaction_safe))
void test_function()
{
  static int counter = 0;

  safe_print("START - counter: %d", counter);

  // Increment the counter, then go to sleep
  for (int i=0; i < 100; ++i)
    ++counter;
  safe_sleep(SLEEP);

  safe_print("END   - counter: %d\n", counter);
}

// Run multiple transactions 
int main(int argc, char *argv[])
{
  if (argc < 2)
    {
      printf("Usage: %s <mode>\n"
	     "mode: 0 = use actual reset counter \n"
	     "      1 = use simulated reset counter \n"
	     "      2 = use dummy system call \n"
	     "      3 = no reset protection \n",
	     argv[0]);
      return 1;
    }

  int mode = atoi(argv[1]);

  for (int i=0; i < 5; ++i)
    {
      if (mode >= 3)
	test_function();
      else
	run_reset_atomic(test_function, mode);
    }

  return 0;
}
