#include <stdlib.h>
#include <stdio.h>
#include "reset-safe.h"

static int counter = 0;

__attribute__((transaction_safe))
void test_function()
{
  for (int i = 0; i < 100; i++)
    counter++;
  safe_print("Counter is %d", counter);
}

int main(int argc, char *argv[])
{
  if (argc < 2)
    {
      printf("Usage: %s <command>\n"
	     "command: 0=actual reset counter; 1=simulated reset counter, 2=dummy syscall\n\n", 
	     argv[0]);
      return 1;
    }
  int mode = atoi(argv[1]);
  
  run_reset_atomic(test_function, mode);
  return 0;
}
